export default function Header() {
  return (
    <header className="p-4 pb-2 border-x-gray-200">
      <h1 className="text-3xl text-center font-bold  text-gray-700">
        Boxy Generator
      </h1>
    </header>
  );
}
