import CalcResult from "./CalcResult"
import CalcButtons from "./CalcButtons"

export default function Calc() {
  return (
    <div>
      <CalcButtons />
      <CalcResult />
    </div>
  )
}
