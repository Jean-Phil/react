import React from "react"
import ReactDOM from "react-dom/client"
import "./index.css"
import App from "./App"
import { store } from "./store"
import { Provider } from "react-redux"
import ThemeProvider from "./context/ThemeProvider.jsx"

const root = ReactDOM.createRoot(document.getElementById("root"))
root.render(
  // On entoure notre application du provider à qui nous allons fournir le store
  <ThemeProvider>
    <Provider store={store}>
      <App />
    </Provider>
  </ThemeProvider>
)
